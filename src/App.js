import React from 'react';
import Navigation from './navigation';
import { 
  Switch,
  Route
} from 'react-router-dom';
import rootRoutes from './rootRoutes';
import './App.css';


function App(props) {
  return (
    <>
    <Navigation />
    <Switch key={props.location.key}>
      {
        rootRoutes.map((route, key) => {
          return (
            <Route key={key} {...route} />
          )
        })
      }
    </Switch>

  </>
  );
}

export default App;
