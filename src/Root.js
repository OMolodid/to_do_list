import React from 'react';
import { Provider } from 'react-redux';
import Navigation from './navigation';
import RootRoutes from './rootRoutes';
import {
	BrowserRouter,
	Switch,
	Route
} from 'react-router-dom';
// import App from './components/app';
import App from './App';
import store from './redux/store';

import './App.css';

const Root = () => {
	return (
		<>
			<Provider store={store}>
				<BrowserRouter>
					<Route path="/" component={App} />
				</BrowserRouter>
			</Provider>
		</>
	);
}

export default Root;

{/* <Navigation />
<Switch key={props.location.key}>
  {
	rootRoutes.map((route, key) => {
	  return (
		<Route key={key} {...route} />
	  )
	})
  }
</Switch> */}