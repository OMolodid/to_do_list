import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { addInputValue} from '../actions';


class TaskInput extends Component {

    state = {
        newValue: ''
    }

    changeHandler = (e) => {
        const { addInputValue } = this.props;
        const { newValue } = this.state;

        addInputValue(newValue);

        this.setState({
            newValue: e.target.value
        })
    }

    render = () => {
        const { newValue } = this.state;
        let { type, placeholder } = this.props;
        
        let { changeHandler } = this;


        return (
            <>
            <input
            type={type}
            value={newValue}
            onChange={changeHandler}
            placeholder={placeholder}
        />
        </>

        )

    }

}


TaskInput.propTypes = {


    placeholder: PropTypes.string,
    handler: PropTypes.func,
    type: PropTypes.oneOf(['text', 'password', 'number']),
    value: PropTypes.oneOfType([
 PropTypes.string,
 PropTypes.any,
]),
};


const mapDispatchToProps = (dispatch) => ({
    addInputValue: (valueInput) => {
        dispatch(addInputValue(valueInput));
 
    }
});

export default connect(null, mapDispatchToProps)(TaskInput);

