import React from 'react';
import { connect } from 'react-redux';
import TaskInput from './TaskInput';
import ToDoItem from './ToDoItem';
import Navigation from '../navigation';

import { addTodo, toggleTodo, deleteTodo, showDone, showUndone } from '../actions';

class App extends React.Component {
    addToStore = () => {
        const { addTodo } = this.props;
        console.log(this.props);

        addTodo();
    }

    toggleTodo = (id) => () => {
        this.props.toggleTodo(id);
    }

    deleteTodo = (id) => () => {
        this.props.deleteTodo(id);
    }

    showDone = (data) => () => {
        this.props.toggleTodo(data);
    }
    showUndone = (data) => () => {
        this.props.toggleTodo(data);
    }

    render() {
        const { addToStore, toggleTodo, deleteTodo } = this;
        const { todos, valueInput } = this.props;

        return (
            <div>
               
                <header>
                    <TaskInput
                        type="text"
                        value={valueInput}
                        placeholder="Type new task"
                    />

                    <button onClick={addToStore}> Add Task </button>
                </header>

                <ul>
                    {
                        todos.map(todo => {
                            return (
                                <ToDoItem
                                    key={todo.id}
                                    item={todo}
                                    action={toggleTodo}
                                    type="checkbox"
                                    buttonaction={deleteTodo}
                                />
                            )
                        })
                    }
                </ul>
            </div>
        )
    }
}

/*
    Redux
*/
const mapStateToProps = (state, ownProps) => ({
    todos: state.todos.data,
})

const mapDispatchToProps = (dispatch) => ({
    addTodo: () => {
        dispatch(addTodo());
            },
    toggleTodo: (id) => {
        dispatch(toggleTodo(id));
            },
    deleteTodo: (id) => {
        dispatch(deleteTodo(id));
            },
     showDone: (data) =>{
        dispatch(showDone(data));
            },
    showUndone: (data) =>{
        dispatch(showUndone(data));
            },
            
});
        
export default connect(mapStateToProps, mapDispatchToProps)(App);