import React from 'react'
import { NavLink } from 'react-router-dom';

import NavRoutes from './rootRoutes';

const Navigation = () => (
    <nav className="nav">
        {
            NavRoutes.map( (route, key) => {
                if( route.path && !route.dontShowInMenu){
                    return(
                        <span key={key} >
                        <NavLink to={route.path}>
                            { route.title}
                        </NavLink> <span>           </span>
                        </span>
                    )
                }
                
            })
        }
    </nav>
);

export default Navigation;