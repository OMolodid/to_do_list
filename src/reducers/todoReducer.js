import {
    ADD_TODO,
    TOGGLE_TODO,
    DELETE_TODO,
    ADD_INPUT_VALUE,
    SHOW_DONE,
    SHOW_UNDONE
} from '../actions';


const postsInitialState = {
    data: [],
    valueInput: ''
};

const todoReducer = (state = postsInitialState, action) => {
    switch (action.type) {

        case ADD_TODO:
            const { valueInput } = state
            const newItem = {
                task: valueInput,
                done: false,
                id: new Date().getTime()
            }

            return {
                ...state,
                data: [...state.data, 
                    newItem
                ]
            }

        case ADD_INPUT_VALUE:
            return {
                ...state,
                valueInput: action.payload
            }


        case TOGGLE_TODO:
            let todos = state.data.map(item => {
                if (item.id === action.payload) {
                    item.done = !item.done;
                }
                return item;
            });

            return {
                ...state,
                data: todos
            }


        case DELETE_TODO:
            let newtodos = state.data.filter(item => {
                if (item.id !== action.payload) {
                    return item;
                }

            });

            return {
                ...state,
                data: newtodos
            }

        case SHOW_DONE:
        let doneTasks = state.data.filter(item => {
            if (item.done) {
                return item;
            }
            
        });

        return {
            ...state,
            data: doneTasks
        }

        case SHOW_UNDONE:
        let undoneTasks = state.data.filter(item => {
            if (!item.done) {
                return item;
            }
            
        });

        return {
            ...state,
            data: undoneTasks
        }




        default:
            return state;
    }
}

export default todoReducer;